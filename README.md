# NodeJS SOAP client

## Requirements
This example is using the local SOAP server described in https://github.com/honestserpent/node-soap-example

## Install application
     npm install
### Run application
#### For testing
     npm test
#### For running 
     npm start
