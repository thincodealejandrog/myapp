$(document).ready(function () {

    $('form.api-response').addClass("loading-box");

    var parameters = { search: $(this).val() };
    $.get( '/searching_cust', function(data) {
        $('form.api-response').removeClass("loading-box");
        $('form.api-response').addClass("display-table");
        json_data = JSON.parse(data).OrderResult;

        if(json_data.statusCode == 0) {

            // Retrieve data from JSON response
            var currency = json_data.data.currency;
            var customerContactID = json_data.data.customerContactID;
            var customerID = json_data.data.customerID;
            var orderDisplayName = json_data.data.orderDisplayName;
            var orderID = json_data.data.orderID;
            var projectManagerID = json_data.data.projectManagerID;
            var rate = json_data.data.rate;
            var requestID = json_data.data.requestID;

            // Displays the info on the screen
            $('form.api-response').append(createTextInputWithLabel("currency", currency, "Currency"));
            $('form.api-response').append(createTextInputWithLabel("customerContactID", customerContactID, "Customer Contact"));
            $('form.api-response').append(createTextInputWithLabel("customerID", customerID, "Customer"));
            $('form.api-response').append(createTextInputWithLabel("orderDisplayName", orderDisplayName, "Order Namer"));
            $('form.api-response').append(createTextInputWithLabel("orderID", orderID, "Order ID"));
            $('form.api-response').append(createTextInputWithLabel("projectManagerID", projectManagerID, "Project Manager"));
            $('form.api-response').append(createTextInputWithLabel("rate", rate, "Rate"));
            $('form.api-response').append(createTextInputWithLabel("requestID", requestID, "Request ID"));

        } else {

            $("p.api-response").html("Error, load the page again");
        }
    });
    
});

function createLabel(inputName, labelText) {
    return $('<label>', { for: inputName, text: labelText });
}

function createTextInput(inputName, textValue) {
    return $('<input>', { type: "text", name: inputName, value: textValue, disabled: true});
}

function createTextInputWithLabel(inputName, textValue, labelText) {
    return [createLabel(inputName, labelText), createTextInput(inputName, textValue)];
}