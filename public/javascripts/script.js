var quote_width = $("article.quote").width();
var no_quotes = $("article.quote").length;

$(document).ready(function () {

    // Sets the elements to the right of their parent
    $("article.quote").each(function(index) {
        $(this).css("left", quote_width*index);
    });

});

$(window).resize( function () {

    quote_width = $("article.quote").width();

    $("article.quote").each(function(index) {
        $(this).css("left", quote_width*index);
    });
});

$(function() {
    setInterval(function() {

        $("article.quote").each(function(index) {
            var current_pos = $(this).position().left;
            if(current_pos < 0)
                current_pos = quote_width*(no_quotes-1);

            $(this).css("left", current_pos - quote_width);
        });

    },5000);
});