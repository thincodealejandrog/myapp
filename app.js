const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const helmet = require('helmet');
const passport = require('passport');
const session = require('express-session');
const flash = require('connect-flash');

var app = express();

// Configures passport
require('./config/passport')(passport); // pass passport for configuration

// helmet
app.use(helmet());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use('/chart.js', express.static(__dirname + '/node_modules/chart.js/dist/'));
app.use(flash());
app.use(session({
  secret: 'somerandonstuffs',
  resave: false,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

// --> Routes
require('./routes/index')(app, passport);
require('./routes/split')(app, passport);
require('./routes/login')(app, passport);
require('./routes/logout')(app, passport);
require('./routes/customers')(app, passport);
require('./routes/user')(app, passport);
require('./routes/upload')(app, passport);
// <-- Routes

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
