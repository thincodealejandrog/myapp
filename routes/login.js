module.exports = function(app, passport) {
  /* Render Login Page if user is not already logged */
  app.get('/login', function(req, res) {

    res.render('login', {
      title: 'Login',
      name: 'Alejandro Guevara',
      data: req.flash('error')
    });
    
  });

  /* Attempt to log in. */
  app.post('/login', passport.authenticate('api_login', {
    successRedirect: '/customers',
    failureRedirect: '/login',
    failureFlash: true
  }));
}