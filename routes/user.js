const User = require('../models/userAPI');

module.exports = function(app, passport) {

  /* Render Login Page if user is not already logged */
  app.get('/user', isAuthenticated, function(req, res) {

    var uuid = req.session.passport.user;

    User.fetchByUserName(uuid, "API", function(err, result) {
      if(err) response = err;
      else response = result;

      res.render('customers', {
        title: 'List of Customers',
        name: 'Alejandro Guevara',
        data: response
      });
    });

  });
}

function isAuthenticated(req, res, next) {
  if (req.isAuthenticated())
    return next();
  req.flash('error', 'You need to authenticate first.');
  res.redirect('/login');
}