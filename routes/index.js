module.exports = function(app, passport) {

  /* Get Home Page */
  app.get('/', isNotAuthenticated, (req, res) => {
    res.render('index', {
      title: "Mi primer Sitio con Pug y NodeJS",
      name: "Alejandro Guevara",
      data: "Eres bienvenido"
    });
  });

}

function isNotAuthenticated(req, res, next) {
  if (!req.isAuthenticated())
    return next();
  res.redirect('/split');
}