const Customer = require('../models/customerAPI');
const Order = require('../models/orderAPI');
const Admin = require('../models/adminAPI')

module.exports = function(app, passport) {

  /* Render Login Page if user is not already logged */
  app.get('/customers', isAuthenticated, function(req, res) {

    var uuid = req.session.passport.user;
    /*Admin.getUserList_Customer(uuid, function(err, result) {
      var response = '';

      if (err) response = err;
      else response = result;

      res.render('customers', {
        title: 'List of Customers',
        name: 'Alejandro Guevara',
        data: response
      });
    });*/
    Order.getOrderObject(uuid, "55013", function(err, result){
      if(err) response = err;
      else response = result;

      res.render('customers', {
        title: 'List of Customers',
        name: 'Alejandro Guevara',
        data: response
      });
    });
  });
}

function isAuthenticated(req, res, next) {
  if (req.isAuthenticated())
    return next();
  req.flash('error', 'You need to authenticate first.');
  res.redirect('/login');
}