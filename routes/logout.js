const plunetAPI = require('../models/plunetAPI');

module.exports = function(app, passport) {

  /* Render Login Page if user is not already logged */
  app.get('/logout', isAuthenticated, function(req, res) {

    plunetAPI.logout(req.session.passport.user, function (err, success) {
      if (err) {
        console.log(err);
        res.render('split', {
          title: 'Type in a string to be splitted',
          name: 'Alejandro Guevara',
          data: 'Failed to log out'
        });
      }
      if (success) {
        req.logout();
        res.redirect('/');
      }
    });

  });
}

function isAuthenticated(req, res, next) {
  if (req.isAuthenticated())
    return next();
  res.redirect('/');
}