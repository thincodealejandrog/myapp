const formidable = require('formidable');
const util = require('util');

module.exports = function(app, passport) {

  app.post('/upload', isAuthenticated, function(req, res) {
    var form = formidable.IncomingForm();
    form.multiples = true;
 
    form.parse(req, function(err, fields, files) {
      res.writeHead(200, {'content-type': 'text/plain'});
      res.write('received upload:\n\n');
      res.end(util.inspect({fields: fields, files: files}));
      console.log(util.inspect({fields: fields, files: files}));
      
    }); 
  });

}

function isAuthenticated(req, res, next) {
  if (req.isAuthenticated())
    return next();
  req.flash('error', 'You need to authenticate first.');
  res.redirect('/login');
}