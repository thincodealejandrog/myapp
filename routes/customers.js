const Customer = require('../models/customerAPI');
const Order = require('../models/orderAPI');
const Admin = require('../models/adminAPI')

module.exports = function(app, passport) {

  /* Render Login Page if user is not already logged */
  app.get('/customers', isAuthenticated, function(req, res) {

    res.render('customers', {
      title: 'List of Customers',
      name: 'Alejandro Guevara'
    });

  });

  app.get('/searching_cust', isAuthenticated, function(req, res) {

    var uuid = req.session.passport.user;

    Order.getOrderObject2(uuid, "55013", function(err, result){
      if (err) res.send(err);

      res.send( JSON.stringify(result) );
    });

  });
}

function isAuthenticated(req, res, next) {
  if (req.isAuthenticated())
    return next();
  req.flash('error', 'You need to authenticate first.');
  res.redirect('/login');
}