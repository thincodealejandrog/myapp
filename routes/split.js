const soap = require('soap');

module.exports = function(app, passport) {

  app.get('/split', isAuthenticated, function(req, res) {

    console.log(req.session.passport.user);

    res.render('split', {
      title: 'Type in a string to be splitted',
      name: 'Alejandro Guevara',
      data: ''
    });

  });
  
  /* GET users listing. */
  app.post('/split', isAuthenticated, function(req, res) {
    var url = 'http://localhost:8000/wsdl?wsdl';

    var MessageSplitterRequest = {
      message: req.body['string'],
      splitter: req.body['splitter']
    };
  
    soap.createClient(url, function(err, client) {
      client.MessageSplitter(MessageSplitterRequest, function(err, result) {
  
        res.render('split', {
          title: 'Type in a new string to be splitted',
          name: 'Alejandro Guevara',
          data: result.result
        });

      });
    });
  });

}

function isAuthenticated(req, res, next) {
  if (req.isAuthenticated())
    return next();
  req.flash('error', 'You need to authenticate first.');
  res.redirect('/login');
}