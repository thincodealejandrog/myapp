const soap = require('strong-soap').soap;
const url = 'https://business.commit-global.com/DataUser25?wsdl';

exports.fetchByUserName = function(uuid, username, done) {
  var args = { UUID: uuid };

  soap.createClient(url, function(err, client) {
    if (err) return done(err);

    client.getUser(args, function(error, result) {

      if (error) return done(error);
      done(null, JSON.stringify(result));

    });
  });

}
// "8952d7e9-9e81-4926-a4cb-3ce8918432ee"
// "4694d032-fdfe-434f-9a00-d531462702e8"