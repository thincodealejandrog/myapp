const soap = require('strong-soap').soap;
const url = 'https://business.commit-global.com/DataUser25?wsdl';

exports.fetchByUserName = function(uuid, username, done) {
  var args = { UUID: uuid, userName: username };

  soap.createClient(url, function(err, client) {
    if (err) return done(err);

    client.fetchByUserName(args, function(error, result) {

      if (error) return done(error);
      done(null, JSON.stringify(result));

    });
  });

}
// "8952d7e9-9e81-4926-a4cb-3ce8918432ee"