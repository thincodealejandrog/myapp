const soap = require('strong-soap').soap;
const url = 'https://business.commit-global.com/DataOrder30?wsdl';

exports.getOrderID = function(uuid, display_no, done) {
  var args = { UUID: uuid, displayNo: display_no };

  soap.createClient(url, function(err, client) {
    if (err) return done(err);

    client.getOrderID(args, function(error, result) {

      if (error) return done(error);
      done(null, JSON.stringify(result));

    });
  });

}

exports.insert = function(uuid, done) {
  var args = { UUID: uuid };

  soap.createClient(url, function(err, client) {
    if (err) return done(err);

    client.insert(args, function(error, result) {

      if (error) return done(error);
      done(null, JSON.stringify(result));

    });
  });
}

exports.getOrderObject2 = function(uuid, order_no, done) {
  var args = { UUID: uuid, orderNumber: order_no };

  soap.createClient(url, function(err, client) {
    if (err) return done(err);

    client.getOrderObject2(args, function(error, result) {
      if(error) return done(error);
      done(null, result);
    });
  });
}
// {"IntegerResult":{"statusCode":"0","statusMessage":"Ok","data":"55013"}}