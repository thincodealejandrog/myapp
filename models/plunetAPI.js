const soap = require('strong-soap').soap;
const url = 'https://business.commit-global.com/PlunetAPI?wsdl';

exports.login = function(username, password, done) {
  var args = { arg0: username, arg1: password };

  soap.createClient(url, function(err, client) {
    if (err) return done(err);

    client.login(args, function(error, result) {
      
      if (error) return done(error);
      done(null, result['return']);

    });
  });
}

exports.logout = function(uuid, done) {
  var args = { arg0: uuid };

  soap.createClient(url, function(err, client) {
    if (err) return done(err);

    client.logout(args, function(error, result) {
      
      if (error) return done(error);
      done(null, true);

    });
  });
}
// "8952d7e9-9e81-4926-a4cb-3ce8918432ee"