const soap = require('strong-soap').soap;
const url = 'https://business.commit-global.com/DataAdmin25?wsdl';

exports.getUserList_Customer = function(uuid, done) {
  var args = { UUID: uuid };

  soap.createClient(url, function(err, client) {
    if (err) return done(err);

    client.getUserList_Customer(args, function(error, result) {

      if (error) return done(error);
      done(null, JSON.stringify(result));

    });
  });

}
// "8952d7e9-9e81-4926-a4cb-3ce8918432ee"