var mysql = require('mysql')
  , async = require('async')

var PRODUCTION_DB = 'users'
  , TEST_DB = 'users'

var connection  = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: TEST_DB
});
connection.connect();

module.exports = connection;