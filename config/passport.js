var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
var PlunetAPI = require('../models/plunetAPI');

module.exports = function(passport) {

  passport.serializeUser(function(id, done) {
    done(null, id);
  });
    
  // used to deserialize the user
  passport.deserializeUser(function(id, done) {
    done(null, id);
  });
    
  passport.use('local', new LocalStrategy(
    function(username, password, done) {
      User.findOneUser(username, password, function(err, user) {

        if (err)
          return done(err);

        if (!user.length)
          return done(null, false, { message: 'Wrong username or password.'});

        // all is well, return successful user
        return done(null, user);
      });
    }
  ));

  passport.use('api_login', new LocalStrategy(
    function(username, password, done) {
      PlunetAPI.login(username, password, function(err, result) {
        if (err)
          return done(err);
        if(result == 'refused')
          return done(null, false, { message: 'Wrong username or password'});
        return done(null, result);
      });
    }
  ));
}